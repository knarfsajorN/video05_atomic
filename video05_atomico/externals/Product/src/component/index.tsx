import React from "React";
import styled from "styled-components";

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width:150px;
`

const Img = styled.img`
    width: 100%;
    aspect-ratio:9/16;
    //height:200px;
    object-fit:cover;
`
const Body = styled.div`
    display:flex;
    flex-direction:column;
    padding-left:10px;
    padding-right:10px;
    background-color:#1c1719;
    border-radius:5px;
`
const Title = styled.p`
    color:white;
`
const Description = styled.p`
    color:white;
`
export interface params{
    value: string
    image: string
    title: string
    description?: string
    onClick? : (value:string) => void
    onMouseLeave? : React.MouseEventHandler<HTMLDivElement>
    onMouseOver?: React.MouseEventHandler<HTMLDivElement> 
} 
const defaultProps = {
    // image : "https://s3.amazonaws.com/imagenes-sellers-mercado-ripley/2021/11/07191856/image-b72a3d893c2446a0bb4f3c066145e103.jpeg",
    // title: "Iphone 13",
    // description: "Apple Description"
}

const App = (params:params):JSX.Element => {
    params = {...defaultProps,...params}

    const handleClick = () => {
        if (typeof params.onClick === 'function') {
            params.onClick(params.value);
        }
    }
    return (
        <Container onClick={handleClick} onMouseLeave={params.onMouseLeave} onMouseOver={params.onMouseOver}>
            <Img src={params.image}/>
            <Body>
                <Title>{params.title}</Title>
                <Description>{params.description}</Description>
            </Body>
        </Container>
    )
}




export default App;