/// <reference types="react" />
import React from "React";
export interface params {
    value: string;
    image: string;
    title: string;
    description?: string;
    onClick?: (value: string) => void;
    onMouseLeave?: React.MouseEventHandler<HTMLDivElement>;
    onMouseOver?: React.MouseEventHandler<HTMLDivElement>;
}
declare const App: (params: params) => JSX.Element;
export default App;
