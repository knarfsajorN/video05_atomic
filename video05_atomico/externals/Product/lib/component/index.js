"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React_1 = __importDefault(require("React"));
const styled_components_1 = __importDefault(require("styled-components"));
const Container = styled_components_1.default.div `
    display: flex;
    flex-direction: column;
    width:150px;
`;
const Img = styled_components_1.default.img `
    width: 100%;
    aspect-ratio:9/16;
    //height:200px;
    object-fit:cover;
`;
const Body = styled_components_1.default.div `
    display:flex;
    flex-direction:column;
    padding-left:10px;
    padding-right:10px;
    background-color:#1c1719;
    border-radius:5px;
`;
const Title = styled_components_1.default.p `
    color:white;
`;
const Description = styled_components_1.default.p `
    color:white;
`;
const defaultProps = {};
const App = (params) => {
    params = Object.assign(Object.assign({}, defaultProps), params);
    const handleClick = () => {
        if (typeof params.onClick === 'function') {
            params.onClick(params.value);
        }
    };
    return (React_1.default.createElement(Container, { onClick: handleClick, onMouseLeave: params.onMouseLeave, onMouseOver: params.onMouseOver },
        React_1.default.createElement(Img, { src: params.image }),
        React_1.default.createElement(Body, null,
            React_1.default.createElement(Title, null, params.title),
            React_1.default.createElement(Description, null, params.description))));
};
exports.default = App;
