import { useState } from 'react'
import Card, {params as paramsCard} from '@react/card'
import CardProduct from '@react/product'
import Grid from '@react/grid'
 

const App = (): JSX.Element => {

  const [data, setData] = useState<paramsCard[]>([
    {value:"1",image:'/vite.svg' ,title:'Tienda',description:'descripcion_test'},
    {value:"2",image:'/vite.svg' ,title:'Tienda2',description:'descripcion_test2'},
    {value:"3",image:'/vite.svg' ,title:'Tienda3',description:'descripcion_test3'},
    {value:"4",image:'/vite.svg' ,title:'Tienda4',description:'descripcion_test4'},
    {value:"5",image:'/vite.svg' ,title:'Tienda5',description:'descripcion_test5'},
    {value:"6",image:'/vite.svg' ,title:'Tienda6',description:'descripcion_test6'},
    {value:"7",image:'/vite.svg' ,title:'Tienda7',description:'descripcion_test7'}

  ])

  const handleSelect:paramsCard['onClick'] = (e) => {
    console.log(e);
  }
  return (
    <Grid >
      <CardProduct 
          value="1" 
          image='https://s3.amazonaws.com/imagenes-sellers-mercado-ripley/2021/11/07191856/image-b72a3d893c2446a0bb4f3c066145e103.jpeg' 
          title='Iphone 13'
          onMouseOver={()=>console.log('over')}
          onMouseLeave={()=>console.log('leave')}/>
      { data.map((v,i)=> <Card {...v} onClick={handleSelect}/>) }
    </Grid>
  )
}

export default App
